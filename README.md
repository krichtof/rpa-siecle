# rpa-siecle


## Description
Rpa-siecle se charge de saisir automatiquement dans Siecle les informations bancaires des responsables légaux des éleves à partir des données saisies dans un formulaire demarches-simplifiees.fr

## Statut du projet
Ce projet est réalisé pour l'instant à titre bénévole. Il avance au rythme du temps que peuvent consacrer les developpeurs qui souhaitent l'améliorer.


## Installation

- installer ruby
- installer chromedriver
- installer rpa-siecle

## Usage

Lancer dans un terminal (Start Command Prompt with Ruby) la commande suivante
```
cd <repertoire dans lequel rpa-siecle est installé>
ruby app.rb <nom-fichier-csv-issu-de-demarches-simplifiees>
```

## Support
Rpa-siecle est un logiciel fourni tel quel. Si l'interface de Siecle change, il peut ne plus fonctionner. Pour toute question, nous vous invitons à rejoindre le forum des utilisateurs OpenAcademie

## Feuille de route

A discuter avec les premiers utilisateurs de RPASiecle


## Contribuer
Vous pouvez proposer votre aide en créant des issues ou des MR ici.


## Auteurs et remerciements
Bientôt ici votre nom si vous contribuez ;)

## Licence

[GNU General Public License v3](./LICENSE)

