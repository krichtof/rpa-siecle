require 'selenium-webdriver'

class RpaSiecle
  THINK_TIME = 3
  attr_reader :driver

  def initialize
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--log-level=3')
    @driver = Selenium::WebDriver.for(:chrome, options: options)
  end

  def login(user, password)
    driver.get 'https://agriates.in.ac-paris.fr/login/ct_logon_mixte.jsp'
    driver.find_element(id: 'user').send_keys(user)
    driver.find_element(id: 'password').send_keys(password)
    driver.find_element(xpath: '//input[@value="Valider"]').click
    think
  end

  def maj(uai)
    driver.find_element(link_text: 'Mise à jour').click
    think
    select_env = driver.find_element(css: 'sco-select-env').shadow_root
    select_etab = select_env.find_element(css: 'sco-select-etab').shadow_root
    select_etab.find_element(css: 'span.si-select-etab-input-caret').click
    select_etab.find_element(css: "li[data-codeuai='#{uai}'").click
    think
    driver.find_element(xpath: '//input[@value="Entrer"]').click
    think
  end

  def recherche(prenom, nom)
    driver.get('https://agriates.in.ac-paris.fr/sconetbe/fiches/ficheIndividuelle/prepareRechercheEleves.do')
    think
    driver.find_element(id: 't_nom').send_keys(nom.strip)
    driver.find_element(id: 't_prenom').send_keys(prenom.strip + "\n")
    think
    begin
      driver.find_element(css: 'table#eleve tr td a').click
    rescue(Selenium::WebDriver::Error::NoSuchElementError)
      puts "Eleve #{prenom} #{nom} non présent dans Siecle."
      return false
    end
    think
    true
  end

  def parcourir_responsables
    driver.find_element(link_text: 'RESPONSABLES').click
    think

    responsables = []
    driver.find_elements(css: "#fiche h3").each_with_index do |resp, index|
      name = resp.find_element(xpath: '..').find_element(css: "h3 ~ div h5").text.split.join(' ')
      responsables << OpenStruct.new(name: name, payeur: payeur?(resp.text), index: index)
    end
    responsables
  end

  def find_current_payeur
    parcourir_responsables.find{|r| r.payeur}
  end

  def select_payeur(titulaire)
    responsables = parcourir_responsables
    payeur = responsables.find{|r| r.payeur}
    new_payeur = responsables.find{|resp| payeur_new_titulaire?(resp, titulaire)}

    if payeur == new_payeur
      puts "ok for #{payeur.inspect} avec titulaire: #{titulaire}.inspect"
      driver.execute_script("document.querySelectorAll('#fiche h3')[#{payeur.index}].querySelector('a').click()")
    else
      puts "#{titulaire} n'est pas le payeur actuel (#{payeur.name})"
      if payeur
        puts "Payeur actuel : #{payeur.name})"
      else
        puts "Aucun payeur actuellement"
      end
      puts "new payeur: #{new_payeur.name}"
      toggle_paie(payeur)
      toggle_paie(new_payeur, false)
    end
    think
    sleep 7
  end

  def toggle_paie(payeur, save=true)
    driver.execute_script("document.querySelectorAll('#fiche h3')[#{payeur.index}].querySelector('a').click()")
    think
    driver.execute_script("document.querySelector('input#respFinancier').click()")
    think
    driver.execute_script("document.querySelector('a#bValider').click()") if save
    think
  end

  def update_paiement(check, infos_bancaires)
    select_payeur(infos_bancaires[:titulaire])
    update_mode_paiement
    update_titulaire(infos_bancaires[:titulaire])
    update_domiciliation(infos_bancaires[:domiciliation])
    update_rib(infos_bancaires[:iban])
    update_bic(infos_bancaires[:bic])
    think

    driver.find_element(link_text: check ? "ANNULER" : "VALIDER").click
    think
  end

  def update_mode_paiement
    select_element = driver.find_element(css: '#paiement #paiementId')
    mode_paiement_select = Selenium::WebDriver::Support::Select.new(select_element)
    mode_paiement_select.select_by(:text, 'VIREMENT BANCAIRE SEPA')
  end

  def update_titulaire(titulaire)
    driver.find_element(css: '#paiement #llCompte').send_keys(titulaire_label(titulaire))
  end

  def titulaire_label(titulaire)
    "#{titulaire[:civilite]} #{titulaire[:nom]} #{titulaire[:prenom]}"
  end

  def update_domiciliation(domiciliation)
  end

  def update_rib(iban)
    clear_iban
    form_group_iban = driver.find_elements(css: '#paiement .form-group').filter do |group|
      /IBAN/ =~ group.text
    end.first

    iban_array = iban.split
    form_group_iban.find_elements(css: 'input').each do |input_iban|
      input_iban.send_keys iban_array.shift
    end
  end

  def clear_iban
    form_group_iban = driver.find_elements(css: '#paiement .form-group').filter do |group|
      /IBAN/ =~ group.text
    end.first

    form_group_iban.find_elements(css: 'input').each do |input_iban|
      input_iban.clear
    end
  end

  def update_bic(bic)
    driver.find_element(css: "#paiement input[name='bic']").send_keys(bic)
  end

  def quit
    driver.quit
  end

  def think
    sleep THINK_TIME
  end

  def maximize
    driver.manage.window.maximize
  end

  private

  def find_parent(element, css_class)
    el = element
    while (el.attribute('class') != css_class && el.tag_name != 'main')
      puts el.attribute('class')
      el = el.find_element(xpath: './..')
    end
    return nil if el.tag_name == 'main'
    el
  end

  def payeur?(text)
    !(/(PAIE LES FRAIS SCOLAIRES)/ =~ text).nil?
  end

  def payeur_new_titulaire?(payeur, titulaire)
    payeur.name.include?(titulaire[:prenom])
  end
end
