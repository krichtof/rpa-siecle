require './rpa_siecle'

class Controller
  attr_reader :siecle, :check

  def initialize(options)
    @siecle = RpaSiecle.new
    @check = options[:check]
    siecle.maximize
    siecle.login(options[:user], options[:password])
    siecle.maj(options[:uai])
  end

  def update_eleve(eleve)
    if siecle.recherche(eleve[:prenom_eleve], eleve[:nom_eleve])
      siecle.update_paiement(check, titulaire: titulaire(eleve), iban: eleve[:iban], bic: eleve[:bic])
    end
  end

  def update_eleves(file)
    eleves = File.open(file) do |csv|
      CSV.table(csv)
    end

    eleves.each{|eleve| update_eleve(eleve)}
  end

  def quit
    siecle.quit
  end


  def titulaire(eleve)
    { civilite: eleve[:civilite], prenom: eleve[:prenom], nom: eleve[:nom] }
  end
end
