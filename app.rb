require './controller'
require 'csv'
require 'tty-prompt'

puts "Merci d'utiliser RPA Siecle !"
puts "Ce logiciel est fourni tel quel. Si l'interface de Siecle change, il peut ne plus fonctionner"
puts "Pour toute question, rejoignez le forum des utilisateurs OpenAcademie"
puts "-----------------"
prompt = TTY::Prompt.new
user = prompt.ask("User Siecle: ", default: ENV["USER_SIECLE"])
password = prompt.mask("Mot de passe Siecle: ")
uai = prompt.ask("UAI etablissement: ", default: ENV["UAI_ETABLISSEMENT"])
eleves = prompt.ask("Fichier Eleves issu de Demarches Simplifiees: ", default: ARGV[0])
check = prompt.yes?("Mode test: ")

controller = Controller.new(user: user, password: password, uai: uai, check: check)
controller.update_eleves(eleves)


